﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Java.Lang;
using Java.Util.Logging;

namespace AndroidApplication6
{
    [Application(Debuggable =  true)]
    public class App : Application
    {
        
    }
    [Activity(Label = "AndroidApplication6", MainLauncher = true, Icon = "@drawable/icon")]
    public class Activity1 : Activity
    {
        int count = 1;
        private static bool _GcKicked;
        private Button _Button;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Window.AddFlags(WindowManagerFlags.KeepScreenOn);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            _Button = FindViewById<Button>(Resource.Id.MyButton);

            _Button.Click += delegate
            { 
                Console.WriteLine("====================");
                _Button.Text = string.Format("{0} clicks!", count++);
                Iter();
                KickGc();
            };
        }

        private void IterSerial() //works
        {
            var values = new List<int>();
            for(var i = 0; i < 20; ++i)
            {
                lock (values)
                {
                    values.Add(i);
                }
            }

            if(count++ % 25 == 0)
                _Button.Text = string.Format("{0} hits! corrupt {1}", count, _Corrupt);
            Task.Run(() => RunOnUiThread(Iter));
        }
        private void Iter() //DeadLocks 4.12.2 stable and 4.12.3
        {
            var values = new List<int>();
            Parallel.For(0, 20, i =>
            {
                lock (values)
                {
                    values.Add(i);
                }
            });
            values.Sort();
            for (var i = 0; i < 20; ++i)
                if (values[i] != i)
                {
                    ++_Corrupt;
                    Console.WriteLine("Corrupt"); //happens once or twice per 10k on my device (already known https://bugzilla.xamarin.com/show_bug.cgi?id=18363)
                    break;
                }
            if (count++ % 25 == 0)
                _Button.Text = string.Format("{0} hits! corrupt {1}", count, _Corrupt);
            Task.Run(() => RunOnUiThread(Iter));
        }
        private void IterNoLock() //Deadlocks
        {
            var values = new ConcurrentQueue<int>();
            Parallel.For(0, 20, i =>
            {
                values.Enqueue(i);
            });
            if (count++ % 25 == 0)
                _Button.Text = string.Format("{0} hits! corrupt {1}", count, _Corrupt);
            Task.Run(() => RunOnUiThread(Iter));
        }

        public int _Corrupt;

        private static void KickGc()
        {
            if (_GcKicked)
                return;
            _GcKicked = true;
            new System.Threading.Thread(() =>
            {
                for (;;)
                {
                    GC.Collect();
                    System.Threading.Thread.Sleep(100);
                }
            }).Start();
        }
    }
}

